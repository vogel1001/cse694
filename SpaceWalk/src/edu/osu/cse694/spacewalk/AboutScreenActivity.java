package edu.osu.cse694.spacewalk;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class AboutScreenActivity extends Activity {

	@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_screen);
		WebView wv = (WebView) findViewById(R.id.about);
		wv.loadUrl("file:///android_asset/space_walk_about");
	}
	
}
