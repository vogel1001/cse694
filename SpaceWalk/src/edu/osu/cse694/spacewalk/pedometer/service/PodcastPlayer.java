package edu.osu.cse694.spacewalk.pedometer.service;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
//import android.util.Log;
import edu.osu.cse694.spacewalk.Planet;

/*
 * singleton class
 */
public class PodcastPlayer {

	/*
	 * 
	 */
	public static String TAG = "PodcastPlayer";
	
	/*
	 * 
	 */
	private static PodcastPlayer instance = null;

	/*
	 * 
	 */
	private static MediaPlayer mp;
	
	/*
	 * 
	 */
	private static int currPlaying;
	
	/*
	 * 
	 */
	private static boolean playing;
	
	/*
	 * 
	 */
	private static int[] times = { 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	/*
	 * 
	 */
	private static Context context;
	
	/*
	 * 
	 * @param context
	 */
	private PodcastPlayer( Context context ) {
		playing = false;
		PodcastPlayer.context = context;
	}

	/*
	 * 
	 * @param context
	 * @return PodcastPlayer
	 */
	public static PodcastPlayer getInstance( Context context ) {
		if (instance == null) {
			instance = new PodcastPlayer(context);
		}
		return instance;
	}

	/*
	 * 
	 * @param planetNum
	 */
	public void playPlanet(int planetNum) {
		// if we are still playing, pause the current planet and start the new
		if(isPlaying()) {
			pausePlanet();
		}

		currPlaying = planetNum;
		mp = null;
		//Log.i("PodcastPlayer", "Playing audio for planet number " + planetNum);
		// plays a planet
		
		mp = MediaPlayer.create(context, Planet.PODCAST_FILES[planetNum]);
		//Log.d("CurrPlayingValue",Integer.toString(currPlaying));
		//Log.d("Time",Integer.toString(times[currPlaying]));
		mp.seekTo(times[currPlaying]);
		playing = true;
		mp.start();
		mp.setOnCompletionListener(new OnCompletionListener() {
			
			public void onCompletion(MediaPlayer mp) {
				//Log.i("PodcastPlayer", "playback complete");
				//mp.release();
				mp.stop();
				times[currPlaying] = 0;
				playing = false;
			}
		});
		
		//Log.d(TAG, "playPlanet " + Integer.toString(planetNum));
	}
	
	/*
	 * 
	 */
	public void pausePlanet() {
		times[currPlaying] = mp.getCurrentPosition();
		//Log.d("PAUSE_PLANET","TIME " + Integer.toString(mp.getCurrentPosition()));
		//Log.d("PAUSE_PLANET","TIME " + Integer.toString(times[currPlaying]));
		mp.stop();
		playing = false;
		//mp.release();
		
		//Log.d(TAG, "pausePlanet");
	}

	/*
	 * 
	 * @param planetNum
	 */
	public void stopPlanet(int planetNum) {
		times[planetNum] = 0;
		if(planetNum == currPlaying) {
			// stop the audio as well as resetting to 0
			mp.stop();
			playing = false;
			//mp.release();
		}
		//Log.d(TAG, "stopPlanet " + Integer.toString(planetNum));
	}
	
	/*
	 * 
	 */
	public void destroy() {
		Log.d("DESTROY","DESTROY PPLAYER");
		for( int i = 0; i < 9; i++ ) {
			times[i] = 0;
		}
		mp.stop();
		mp.release();
	}
	
	/*
	 * 
	 * @return boolean
	 */
	public boolean isPlaying() {
		//Log.d(TAG, "isPlaying");
		return playing;
	}
	
	/*
	 * 
	 * @return int
	 */
	public int getPlayingPlanet() {
		//Log.d(TAG, "getPlayingPlanet");
		return currPlaying;
	}
}
