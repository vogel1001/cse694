/*
 *  Pedometer - Android App
 *  Copyright (C) 2009 Levente Bagi
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.osu.cse694.spacewalk.pedometer.service;

import static edu.osu.cse694.spacewalk.SettingsActivity.KEY_DISTANCE_TO_TRAVEL_PREFERENCE;
import static edu.osu.cse694.spacewalk.SettingsActivity.KEY_END_PLANET_PREFERENCE;
import static edu.osu.cse694.spacewalk.SettingsActivity.KEY_START_PLANET_PREFERENCE;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.Time;

/**
 * Used to access settings from the start screen
 */
public class PedometerSettings {

	/*
	 * The sharedpreferences to pull information from
	 */
	SharedPreferences mSettings;
	
    /**
     * Starts a new PedometerSettings to pull information from
     * @param context the context to get the settings from
     */
    public PedometerSettings(Context context) {
    	mSettings = PreferenceManager.getDefaultSharedPreferences(context);
    }
    
    /**
     * Gets the start planet number, as specified by the user
     * @return the starting planet as denotated in Planet.java
     */
    public int getStartPlanet() {  
    	return Integer.parseInt(mSettings.getString(KEY_START_PLANET_PREFERENCE, "0"));
    }
    
    /**
     * Gets the end planet number, as specified by the user
     * @return the ending planet as denotated in Planet.java
     */
    public int getEndPlanet() {
    	return Integer.parseInt(mSettings.getString(KEY_END_PLANET_PREFERENCE, "8"));
    }
    
    /**
     * Returns the total distance (in earth miles) the user wishes to walk
     * @return the distance the user wishes to walk
     */
    public float getDistance() {
    	return Float.parseFloat((mSettings.getString(KEY_DISTANCE_TO_TRAVEL_PREFERENCE, "2")));
    }
    
    /**
     * Returns weather or not the user wishes to have the podcast audio playing
     * @return weather the user wishes to have the podcast audio playing.
     */
    public boolean playAudio() {
    	return mSettings.getBoolean("planet_audio", true);
    }
}
