package edu.osu.cse694.spacewalk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
//import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * When a user arrives at a planet, this congratualatory splash screen is presented.
 */
public class ArrivedAtPlanetSplashActivity extends Activity {

	/*
	 * splashWait: Number of ms to wait on the splashscreen
	 */
	private static final int splashWait = 5000;

	/*
	 * launchMain: The runnable that will launch the main screen
	 */
	private Runnable launchMain;
	
	/*
	 * launchMainHandler: The handler responsable to starting launchmain
	 */
	private Handler launchMainHandler;

	/*
	 * The planet that has been reached.  Informs the display.
	 */
	private int planetArrivedAt;
	
	/*
	 * Determines if the phone will vibrate
	 */
	private SharedPreferences settings;

	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		//Log.d("SPLASH", "SPLASH");
		setContentView(R.layout.arrived_at_planet_splash);
		settings = PreferenceManager.getDefaultSharedPreferences(this);
		launchMainHandler = new Handler();

		planetArrivedAt = getIntent().getExtras().getInt("arrival");
		int picture_file = Planet.LARGE_PICTURE_FILES[planetArrivedAt];

		ImageView planetImage = (ImageView) findViewById(R.id.planet_image);
		planetImage.setImageResource(picture_file);

		TextView planetName = (TextView) findViewById(R.id.planet_splash_name);
		planetName.setText(Planet.PLANET_NAMES[planetArrivedAt]);

		launchMain = new Runnable() {
			public void run() {
				launchNextActivity();
			}
		};
		if (!settings.getString(SettingsActivity.NOTIFICATION, "0").equals("0")) {
			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

			// Vibrate for 300 milliseconds
			v.vibrate(800);
		}
		launchMainHandler.postDelayed(launchMain, splashWait);

	}

	/**
	 * If this isn't the last planet reached, the regular WalkingActivity screen will be returned to; if the walk has been
	 * completed, an additional end-of-walk splash screen will appear
	 */
	public void launchNextActivity() {
		if( planetArrivedAt < Integer.parseInt(settings.getString(SettingsActivity.KEY_END_PLANET_PREFERENCE,"0")) ) {
			Intent launch = new Intent(this, WalkingActivity.class);
			launch.putExtra("pager_tab_number", planetArrivedAt);
			startActivity(launch);
			finish();
		} else {
			Intent launch = new Intent(this, WalkCompleteSplash.class);
			launch.putExtra("pager_tab_number", planetArrivedAt);
			startActivity(launch);
			finish();
		}
	}

	/**
	 * 
	 *
	 */
	public void onPause() {
		super.onPause();
		// remove the timer from the running stack so we don't call the main
		// program after the user has exited the splash screen.
		launchMainHandler.removeCallbacks(launchMain);
		finish();
	}

	/**
	 * The splash screen will be destroyed on a finger touch
	 * @param event  a finger touch
	 */
	public boolean onTouchEvent(MotionEvent event) {
		// if the user touches the screen, skip right to the main activity
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			launchMainHandler.removeCallbacks(launchMain);
			launchNextActivity();
		}
		return true;
	}

}