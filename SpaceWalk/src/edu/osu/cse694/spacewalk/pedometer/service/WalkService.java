/*
 *  Pedometer - Android App
 *  Copyright (C) 2009 Levente Bagi
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.osu.cse694.spacewalk.pedometer.service;

import com.google.android.maps.GeoPoint;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import edu.osu.cse694.spacewalk.R;
import edu.osu.cse694.spacewalk.WalkingActivity;

/**
 * Large portions of this class is based on black-box code adapted to fit our
 * needs.
 * 
 * This class is the service used to count steps and eventually update the
 * activity.
 */
public class WalkService extends Service {

	/*
	 * The tag for the walk preference for accessing the shared preferences
	 */
	private static final String CONTINUE_WALK_SHARED_PREFERENCE = "ContinueWalk";

	/*
	 * Tag used when retrieving the preference which checks to see if this walk
	 * is a continued walk (versus a new one)
	 */
	private static final String CONTINUE_WALK = "continue_walk";

	/*
	 * Tag used when retrieving the preference which gets the current planet if
	 * continuing a walk
	 */
	private static final String PLANET = "planet";

	/*
	 * Tag used when retrieving the preference which gets the current progress
	 * if continuing a walk
	 */
	private static final String PROGRESS = "progress";

	/*
	 * Tag used when retrieving the preference which gets the current distance
	 * if continuing a walk
	 */
	private static final String DISTANCE = "distance";

	/*
	 * Tag used to identify Log statements from the WalkService
	 */
	private static final String TAG = "WalkService";

	/*
	 * The settings class which can be used to get preferences. This is fed into
	 * the pedometersettings.
	 */
	private SharedPreferences mSettings;

	/*
	 * Allows easy access to the SharedPreferences found in mSettings (defined
	 * above)
	 */
	private PedometerSettings mPedometerSettings;

	/*
	 * Gets the shared preferences which allow access to the previous state
	 * (from a continue walk)
	 */
	private SharedPreferences mState;

	/*
	 * Allows access to the sensor data. Is passed into the StepDetector
	 */
	private SensorManager mSensorManager;

	/*
	 * The sensor the sensormanager detects on
	 */
	private Sensor mSensor;

	/*
	 * Code which detects when a step has been made
	 */
	private StepDetector mStepDetector;

	/*
	 * Notifier which connects with the base activity to inform of a change in distance traveled
	 */
	private DistanceNotifier mDistanceNotifier;

	/*
	 * Notifier which connects with the base activity to inform of a change in planet
	 */
	private PlanetNotifier mPlanetNotifier;

	/*
	 * Manages the above notifications
	 */
	private NotificationManager mNM;

	/*
	 * The list of real world GPS locations for each planet
	 */
	private GeoPoint[] planetLocationsOnEarth = new GeoPoint[9];

	
	/*
	 * 
	 */
	private int mSteps;

	/*
	 * 
	 */
	private float mDistance;

	/*
	 * 
	 */
	private float mProgress;

	/*
	 * 
	 */
	private int mPlanet;

	/*
	 * 
	 */
	protected LocationManager locationManager;

	/*
	 * 
	 */
	protected LocationListener locationListener;

	/*
	 * 
	 */
	public static boolean isRunning = false;

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class StepBinder extends Binder {
		public WalkService getService() {
			Log.i(TAG, "Inside StepBinder");
			return WalkService.this;
		}
	}

	/**
	 * 
	 */
	@Override
	public void onCreate() {

		SharedPreferences continueWalk = getSharedPreferences(
				CONTINUE_WALK_SHARED_PREFERENCE, MODE_PRIVATE);

		boolean contWalk = continueWalk.getBoolean(CONTINUE_WALK, false);

		isRunning = true;
		Log.i(TAG, "onCreate");
		super.onCreate();

		// mPlayer = PodcastPlayer.getInstance();
		// mPlayer.setService(this);

		mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		showNotification();

		// Load settings
		mSettings = PreferenceManager.getDefaultSharedPreferences(this);
		mPedometerSettings = new PedometerSettings(this);
		mState = getSharedPreferences("state", 0);

		// Start detecting
		mStepDetector = new StepDetector();
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		registerDetector();

		// start GPS
		locationListener = new MyLocationListener();

		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				10, 10, locationListener);

		Log.d("WalkService On Create", "Location Manager complete");

		// Register our receiver for the ACTION_SCREEN_OFF action. This will
		// make our receiver
		// code be called whenever the phone enters standby mode.
		IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
		registerReceiver(mReceiver, filter);

		mPlanetNotifier = new PlanetNotifier(mProgressListener,
				mPlanetListener, mPedometerSettings);

		mDistanceNotifier = new DistanceNotifier(mDistanceListener);
		if (contWalk) {
			mDistanceNotifier.setDistance(mDistance = continueWalk.getFloat(
					DISTANCE, 0.0F));
			mPlanetNotifier.setDistance(mDistance);
		} else {
			mDistanceNotifier.setDistance(mDistance = mState.getFloat(DISTANCE,
					0));
		}
		mStepDetector.addStepListener(mDistanceNotifier);

		mPlanetNotifier.setPlanet(mPedometerSettings.getStartPlanet());
		if (contWalk) {
			mPlanetNotifier.setExactProgress(mProgress = continueWalk.getFloat(
					PROGRESS, 0.0F));
		} else {
			mPlanetNotifier.setProgress(mProgress = mState
					.getFloat(PROGRESS, 0));
		}
		mStepDetector.addStepListener(mPlanetNotifier);

		if (contWalk) {
			mPlanetNotifier.setPlanet(mPlanet = continueWalk.getInt(PLANET, 0));
		}

		Log.d("ContWalk", Boolean.toString(contWalk));
		// mDistance = continueWalk.getFloat("distance", 0.0F);
		Log.d("get_distance", Float.toString(mDistance));

		// mPlanet = continueWalk.getInt("planet", 0);
		Log.d("get_planet", Float.toString(mPlanet));

		// mProgress = continueWalk.getFloat("progress", 0.0F);
		Log.d("get_progress", Float.toString(mProgress));

		// mSteps = continueWalk.getInt("steps", 0);
		Log.d("get_steps", Float.toString(mSteps));
		// Start voice

		mPlanetNotifier.reloadSettings();
		reloadSettings();

		// Tell the user we started.
		Toast.makeText(this, getText(R.string.started), Toast.LENGTH_SHORT)
				.show();
		Log.i(TAG, "after toast");
	}

	/**
	 * 
	 * 
	 *
	 */
	private class MyLocationListener implements LocationListener {

		/**
		 * 
		 * @param location
		 */
		public void onLocationChanged(Location location) {
		}

		/**
		 * 
		 * @param s
		 * @param i
		 * @param b
		 */
		public void onStatusChanged(String s, int i, Bundle b) {
		}

		/**
		 * 
		 * @param s
		 */
		public void onProviderDisabled(String s) {
		}

		/**
		 * 
		 * @param s
		 */
		public void onProviderEnabled(String s) {
		}

	}

	/**
	 * 
	 * @param intent
	 * @param startId
	 */
	@Override
	public void onStart(Intent intent, int startId) {
		Log.i(TAG, "onStart");
		super.onStart(intent, startId);
	}

	/**
	 * 
	 * @return int
	 */
	public int getPlanet() {
		return mPlanetNotifier.getPlanet();
	}

	/**
	 * 
	 * @return int
	 */
	public int getProgress() {
		return (int) mPlanetNotifier.getProgress() * 1000;
	}

	/**
	 * 
	 * @param planetId
	 */
	private void addPointToLocationArray(int planetId) {
		Location location = locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		int num_mins = 2;

		if (location != null && System.currentTimeMillis() - location.getTime() < num_mins * 60000) {

			double lat = location.getLatitude();
			double lng = location.getLongitude();
			GeoPoint currPosition = new GeoPoint((int) (lat * 1E6),
					(int) (lng * 1E6));
			planetLocationsOnEarth[planetId] = currPosition;

			String message = String
					.format("Adding Location \n Longitude: %s \n Latitude: %s \n to Planet: %s",
							location.getLongitude(), location.getLatitude(),
							planetId);
			// Log.d("LOCATION",
			// ""+location.getLatitude()+","+location.getLongitude());
			Toast.makeText(WalkService.this, message, Toast.LENGTH_SHORT)
					.show();

			Log.d("Location Not Null", "Toast created.");
		}
	}

	/**
	 * returns an array of geopoints representing the location of the planets on
	 * earth
	 * 
	 * @return GeoPoint[]
	 */
	public GeoPoint[] getPlanetLocationsOnEarth() {
		return planetLocationsOnEarth;
	}

	/**
	 * 
	 */
	@Override
	public void onDestroy() {
		Log.i(TAG, "onDestroy");

		// if audio is playing, stop the audio
		/*
		 * if (mPlayer.isPlaying()) {
		 * mPlayer.stopPlanet(mPlayer.getPlayingPlanet()); }
		 */
		// Unregister our receiver.

		unregisterReceiver(mReceiver);
		unregisterDetector();

		isRunning = false;

		mNM.cancel(R.string.app_name);

		super.onDestroy();

		// Stop detecting
		mSensorManager.unregisterListener(mStepDetector);

		// Tell the user we stopped.
		Toast.makeText(this, getText(R.string.stopped), Toast.LENGTH_SHORT)
				.show();

		locationManager.removeUpdates(locationListener);

		Toast.makeText(this, "GPS updates stopped.", Toast.LENGTH_SHORT).show();
	}

	/**
	 * 
	 */
	private void registerDetector() {
		mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(mStepDetector, mSensor,
				SensorManager.SENSOR_DELAY_FASTEST);
	}

	/**
	 * 
	 */
	private void unregisterDetector() {
		mSensorManager.unregisterListener(mStepDetector);
	}

	/**
	 * 
	 * @param intent
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG, "onBind");
		return mBinder;
	}

	/*
	 * Receives messages from activity.
	 */
	private final IBinder mBinder = new StepBinder();

	/**
	 * 
	 *
	 */
	public interface ICallback {

		/**
		 * 
		 * @param value
		 */
		public void stepsChanged(int value);

		/**
		 * 
		 * @param value
		 */
		public void distanceChanged(float value);

		/**
		 * 
		 * @param value
		 */
		public void planetChanged(int value);

		/**
		 * 
		 * @param value
		 */
		public void progressChanged(float value);

	}

	/*
	 * 
	 */
	private ICallback mCallback;

	/**
	 * 
	 * @param cb
	 */
	public void registerCallback(ICallback cb) {
		mCallback = cb;
	}

	/**
	 * 
	 */
	public void reloadSettings() {
		mSettings = PreferenceManager.getDefaultSharedPreferences(this);

		if (mStepDetector != null) {
			mStepDetector.setSensitivity(Float.valueOf(mSettings.getString(
					"sensitivity", "10")));
		}

		if (mDistanceNotifier != null)
			mDistanceNotifier.reloadSettings();

		if (mPlanetNotifier != null)
			mPlanetNotifier.reloadSettings();
	}

	/**
	 * 
	 */
	public void resetValues() {
		mDistanceNotifier.setDistance(0);
		mPlanetNotifier.setPlanet(0);
		mPlanetNotifier.setProgress(0);
	}

	/*
	 * 
	 */
	private PlanetNotifier.Listener mProgressListener = new PlanetNotifier.Listener() {

		/**
		 * 
		 * @param value
		 */
		public void valueChanged(float value) {
			mProgress = value;
			passValue();
		}

		/**
		 * 
		 */
		public void passValue() {
			if (mCallback != null) {
				mCallback.progressChanged(mProgress);
			}
		}
	};

	/*
	 * 
	 */
	private PlanetNotifier.numListener mPlanetListener = new PlanetNotifier.numListener() {

		/**
		 * 
		 * @param value
		 */
		public void valueChanged(int value) {
			mPlanet = value;
			addPointToLocationArray(mPlanet);
			passValue();
		}

		/**
		 * 
		 */
		public void passValue() {
			if (mCallback != null) {
				mCallback.planetChanged(mPlanet);
			}
		}
	};

	/*
	 * Forwards distance values from DistanceNotifier to the activity.
	 */
	private DistanceNotifier.Listener mDistanceListener = new DistanceNotifier.Listener() {

		/**
		 * 
		 * @param value
		 */
		public void valueChanged(float value) {
			mDistance = value;
			passValue();
		}

		/**
		 * 
		 */
		public void passValue() {
			if (mCallback != null) {
				mCallback.distanceChanged(mDistance);
			}
		}
	};

	/**
	 * Show a notification while this service is running.
	 */
	private void showNotification() {
		CharSequence text = getText(R.string.app_name);

		Notification notification = new Notification(
				R.drawable.ic_notification, null, System.currentTimeMillis());
		notification.flags = Notification.FLAG_NO_CLEAR
				| Notification.FLAG_ONGOING_EVENT;
		Intent pedometerIntent = new Intent();
		pedometerIntent.setComponent(new ComponentName(this,
				WalkingActivity.class));
		pedometerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				pedometerIntent, 0);
		notification.setLatestEventInfo(this, text,
				getText(R.string.notification_subtitle), contentIntent);

		mNM.notify(R.string.app_name, notification);
	}

	/*
	 * BroadcastReceiver for handling ACTION_SCREEN_OFF.
	 */
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {

		/**
		 * 
		 * @param context
		 * @param intent
		 */
		@Override
		public void onReceive(Context context, Intent intent) {
			// Check action just to be on the safe side.
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				// Unregisters the listener and registers it again.
				WalkService.this.unregisterDetector();
				WalkService.this.registerDetector();
			}
		}
	};

	/**
	 * 
	 * @return int
	 */
	public int getCurrentSteps() {
		return mSteps;
	}

	/**
	 * 
	 * @return float
	 */
	public float getCurrentDistance() {
		return mDistance;
	}

	/**
	 * 
	 * @return float
	 */
	public float getCurrentProgress() {
		return mProgress;
	}

	/**
	 * 
	 * @return int
	 */
	public int getCurrentPlanet() {
		return mPlanet;
	}

	/**
	 * 
	 * @param steps
	 */
	public void setCurrentSteps(int steps) {
		mSteps = steps;
	}

	/**
	 * 
	 * @param distance
	 */
	public void setCurrentDistance(float distance) {
		mDistance = distance;
	}

	/**
	 * 
	 * @param progress
	 */
	public void setCurrentProgress(float progress) {
		mProgress = progress;
	}

	/**
	 * 
	 * @param planet
	 */
	public void setCurrentPlanet(int planet) {
		mPlanet = planet;
	}

}
