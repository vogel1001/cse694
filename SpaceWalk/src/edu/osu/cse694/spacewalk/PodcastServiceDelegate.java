package edu.osu.cse694.spacewalk;

import edu.osu.cse694.spacewalk.pedometer.service.PlanetPodcastService;
import edu.osu.cse694.spacewalk.pedometer.service.PodcastPlayer;
import android.content.Context;
import android.content.Intent;
//import android.util.Log;
import android.util.Log;

/**
 * The agent of the PlanetPodcastService (a service that plays audio using the PodcastPlayer).  The WalkingActivity uses this to
 * request podcast functions.  This is a singleton and static class, in order to ensure a consistent state throughout a walk.
 * 
 * PlanetPodcastService is not a persistent service; it is started and stopped as needed.
 */
public class PodcastServiceDelegate {
	
	/*
	 * True if a podcast is currently running
	 */
	private static boolean serviceIsRunning = false;
	
	/*
	 * The planet whose podcast is currently playing
	 */
	private static int planetPodcastCurrentlyPlaying;
		
	/*
	 * WalkingActivity needs to pass in its context; this context will be passed
	 * along to the PlanetPodcastService
	 */
	private static Context context;
	
	/*
	 * Debugging tag
	 */
	private final String TAG = "PodcastServiceDelegate";
	
	/*
	 * The singleton object of this class
	 */
	private static PodcastServiceDelegate delegate;
	
	/**
	 * Private constructor
	 * @param context  the context of the activity using this class (WalkingActivity)
	 */
	private PodcastServiceDelegate( Context context ) {	
		PodcastServiceDelegate.context = context;
		//this.podcastPlayer = PodcastPlayer.getInstance();
	}
	
	/**
	 * Returns the singleton object of this class
	 * @param context  the context of the activity using this class (WalkingActivity)
	 * @return PodcastServiceDelegate  the singleton object of this class
	 */
	public static PodcastServiceDelegate getPodcastServiceDelegate( Context context ) {
		if( delegate == null ) {
			delegate = new PodcastServiceDelegate(context);
		}
		return delegate;
	}
	
	/**
	 * Toggles the state of the audio of the supplied planet; playing audio is paused, and
	 * paused or stopped audio is played
	 * @param planetNum  the planet whose audio will be toggled
	 */
	public void togglePlayPodcast(int planetNum) {
		Log.d("Service is running: " + Boolean.toString(serviceIsRunning),"PSDelegate");
		Log.d("planetPodcastCurrentlyPlaying: " + Integer.toString(planetPodcastCurrentlyPlaying),
				"planetNum: " + Integer.toString(planetNum));
		if (serviceIsRunning && (planetPodcastCurrentlyPlaying == planetNum) ) {
			Log.d("Toggle pause: " + Integer.toString(planetNum), "PSDelegate" );
			pausePlanet();
		} else {
			Log.d("Toggle play: " + Integer.toString(planetNum), "PSDelegate" );
			playPlanet(planetNum);
		}
	}
	
	/**
	 * Plays the audio of the supplied planet
	 * @param planetNum  the planet whose audio will be played
	 */
	public void playPlanet(int planetNum) {
		Intent intent = new Intent( context, PlanetPodcastService.class );
		intent.putExtra("playPlanet",Integer.toString(planetNum));
		planetPodcastCurrentlyPlaying = planetNum;
		serviceIsRunning = true;
		context.startService(intent);
		//Log.d(TAG,"playPlanet " + Integer.toString(planetNum));
	}
	
	/**
	 * Pauses any audio
	 */
	public void pausePlanet() {
		Intent intent = new Intent( context, PlanetPodcastService.class );
		context.stopService(intent);
		
		intent = new Intent( context, PlanetPodcastService.class );
		intent.putExtra("pausePlanet", "noValueNeeded");
		serviceIsRunning = false;
		context.startService(intent);
		
		//Log.d(TAG,"pausePlanet");
	}

	/**
	 * Stops the audio of the supplied planet
	 * @param planetNum  the planet whose audio will be stopped
	 */
	public void stopPlanet(int planetNum) {
		Intent intent = new Intent( context, PlanetPodcastService.class );
		context.stopService(intent);
		
		intent = new Intent( context, PlanetPodcastService.class );
		intent.putExtra("stopPlanet", Integer.toString(planetNum));
		serviceIsRunning = false;
		context.startService(intent);		
				
		//Log.d(TAG,"stopPlanet " + Integer.toString(planetNum));
	}
	
	/**
	 * Returns true if any audio is currently playing.  (This will also reflect if PlanetPodcastService is running).
	 * @return boolean  Returns true if audio is playing
	 */
	public boolean isPlaying() {
		return serviceIsRunning;
	}
	
	/**
	 * Returns the planet whose audio is playing.  (Note: this method, as it is, should be used in conjunction
	 * with isPlaying()).
	 * @return int  the planet whose audio is planet
	 */
	public int getPlayingPlanet() {
		return planetPodcastCurrentlyPlaying;
	}
	
	/**
	 * Destroys PlanetPodcastService; called when the walk is over
	 */
	public void destroy() {
		Intent intent = new Intent( context, PlanetPodcastService.class );
		intent.putExtra("destroy", "0");
		context.startService(intent);
		context.stopService(intent);
	}
}
