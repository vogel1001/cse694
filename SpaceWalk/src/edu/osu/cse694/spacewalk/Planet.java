package edu.osu.cse694.spacewalk;

/**
 * 
 */
public class Planet {
	
	/*
	 * Audio files played when a planet is reached, or when played on a planet's data sheet
	 */
	public static int[] PODCAST_FILES = { R.raw.sun, R.raw.mercury,
			R.raw.venus, R.raw.earth, R.raw.mars, R.raw.jupiter, R.raw.saturn,
			R.raw.uranus, R.raw.neptune };
	
	/*
	 * Planet data sheets
	 */
	public static String[] HTML_FILES = { "sun.html", "mercury.html",
			"venus.html", "earth.html", "mars.html", "jupiter.html",
			"saturn.html", "uranus.html", "neptune.html" };
	
	/*
	 * Pinned to the map when a planet has been reached
	 */
	public static int[] SMALL_PICTURE_FILES = { R.drawable.sun_thumbnail,
			R.drawable.mercury_thumbnail, R.drawable.venus_thumbnail,
			R.drawable.earth_thumbnail, R.drawable.mars_thumbnail,
			R.drawable.jupiter_thumbnail, R.drawable.saturn_thumbnail,
			R.drawable.uranus_thumbnail, R.drawable.neptune_thumbnail };
	
	/*
	 * Planet names
	 */
	public static String[] PLANET_NAMES = { "Sun", "Mercury", "Venus", "Earth",
			"Mars", "Jupiter", "Saturn", "Uranus", "Neptune" };
	
	/*
	 * Pictures used in the data sheets, ArrivedAtPlanetSplashActivity and Upcoming Planet tab
	 */
	public static int[] LARGE_PICTURE_FILES = { R.drawable.sun_large,
			R.drawable.mercury_large, R.drawable.venus_large,
			R.drawable.earth_large, R.drawable.mars_large,
			R.drawable.jupiter_large, R.drawable.saturn_large,
			R.drawable.uranus_large, R.drawable.neptune_large }; // TODO

	/*
	 * Planets represented as ints
	 */
	public static final int SUN = 0;
	public static final int MERCURY = 1;
	public static final int VENUS = 2;
	public static final int EARTH = 3;
	public static final int MARS = 4;
	public static final int JUPITER = 5;
	public static final int SATURN = 6;
	public static final int URANUS = 7;
	public static final int NEPTUNE = 8;
}
