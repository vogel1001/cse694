package edu.osu.cse694.spacewalk;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * The first screen viewed after the title splash screen.  
 */
public class SpaceWalkActivity extends Activity implements OnClickListener {

	/*
	 * The 'New Walk' button; 'Continue Walk' button; 'About Screen' button
	 */
	private Button newButton, continueButton, aboutScreenButton;

	/*
	 * This boolean attribute of the CONTINUE_WALK_SHARED_PREFERENCE (see below) is set to true
	 * if there exists a walk to be continued.  This is false if the walk of the app's previous
	 * run was completed (in which case there is no walk to continue).
	 */
	private static final String CONTINUE_WALK = "continue_walk";
	
	/*
	 * This preference contains data about the unfinished walk of the app's previous run.  This data is
	 * used to restore the walk if the user choose "Continue Walk".
	 */
	private static final String CONTINUE_WALK_SHARED_PREFERENCE = "ContinueWalk";

	
	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start_screen);

		// get buttons
		newButton = (Button) findViewById(R.id.newButton);
		continueButton = (Button) findViewById(R.id.continueButton);
		aboutScreenButton = (Button) findViewById(R.id.about_button);

		// set the onClickListener for new walk
		newButton.setOnClickListener(this);
		aboutScreenButton.setOnClickListener(this);
		
		SharedPreferences continueWalk = getSharedPreferences(CONTINUE_WALK_SHARED_PREFERENCE,
				MODE_PRIVATE);
		boolean continueWalkButtonAppears = continueWalk.getBoolean(CONTINUE_WALK, false);
		if( continueWalkButtonAppears ) {
			// set the onClickListener for continue walk
			continueButton.setOnClickListener(this);
		} else {
			//If the user completed a walk last time, we don't want to give them 
			//the option of continuing the walk
			continueButton.setVisibility(View.GONE);
		}

	}

	/**
	 * 
	 * @param src
	 */
	public void onClick(View src) {
		switch (src.getId()) {
		case R.id.newButton:
			newWalk();
			finish();
			break;
		case R.id.continueButton:
			continueWalk();
			finish();
			break;
		case R.id.about_button:
			aboutScreen();
			break;
		}
	}

	/**
	 * Start a new walk
	 */
	public void newWalk() {
		SharedPreferences continueWalk = getSharedPreferences("ContinueWalk",
				MODE_PRIVATE);
		SharedPreferences.Editor editor = continueWalk.edit();
		editor.putBoolean("continue_walk", false);
		editor.commit();
		startActivity(new Intent(this, SettingsActivity.class));
	}

	/**
	 * Continue the previous walk
	 */
	public void continueWalk() {
		Intent walkingActivityIntent = new Intent(this, WalkingActivity.class);
		SharedPreferences continueWalk = getSharedPreferences("ContinueWalk",
				MODE_PRIVATE);
		SharedPreferences.Editor editor = continueWalk.edit();
		editor.putBoolean("continue_walk", true);
		editor.commit();
		walkingActivityIntent.putExtra("pager_tab_number",
				continueWalk.getInt("planet", 0));
		startActivity(walkingActivityIntent);
	}
	
	/**
	 * About screen
	 */
	public void aboutScreen() {
		startActivity(new Intent(this, AboutScreenActivity.class));
	}
}