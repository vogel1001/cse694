package edu.osu.cse694.spacewalk.pedometer.service;

import android.util.Log;

/**
 * Checks progress toward getting to a new planet
 * 
 * @author David Burl
 * 
 */
public class PlanetNotifier implements StepListener {
	/*
	 * average step length (in inches)
	 */
	public static float STEP_LENGTH = 25;

	/*
	 * this listener listens to steps
	 */
	public interface Listener {
		public void valueChanged(float value);

		public void passValue();
	}

	/*
	 * this listener listens to the planet changes
	 */
	public interface numListener {
		public void valueChanged(int value);

		public void passValue();
	}

	/*
	 * listens to the change in steps
	 */
	private Listener sListener;

	/*
	 * listens to change in planets
	 */
	private numListener pListener;

	/*
	 * Used to retrieve settings
	 */
	private PedometerSettings mSettings;

	/*
	 * The walk status
	 */
	private Walk mWalk;

	/*
	 * PlanetNotifier gets a step and performs functions accordingly (such as
	 * notifying the main activity when that step lines up with a event)
	 * 
	 * @param stepListener Used to listen for step changes
	 * 
	 * @param planetListener Used to listen for planet changes
	 * 
	 * @param settings Used to retrieve settings.
	 */
	public PlanetNotifier(Listener stepListener, numListener planetListener,
			PedometerSettings settings) {
		mSettings = settings;
		sListener = stepListener;
		pListener = planetListener;
		mWalk = new Walk(mSettings.getDistance(),
				mSettings.getStartPlanet(), mSettings.getEndPlanet());
		pListener.valueChanged(mWalk.getPlanetOn());

	}

	/*
	 * Sets the current distance.
	 * May be used if restoring from an old state.
	 * 
	 * @param dist The distance
	 */
	public void setDistance(float dist) {
		mWalk.setDistance(dist);
	}

	/*
	 *Sets the current progress
	 *May be used if restoring from an old state
	 * @param prog The progrss to the next planet;
	 */
	public void setProgress(float prog) {
		mWalk.setDistance(prog);
		sListener.valueChanged(mWalk.getPercentage());

	}

	/*
	 * Update the progress on the application side 
	 * @param prog The progress to show
	 */
	public void setExactProgress(float prog) {
		sListener.valueChanged(mWalk.getPercentage());
	}

	/*
	 * Update the planet
	 * May be used when restoring from an old state
	 * @param planet The planet the user is currently on
	 */
	public void setPlanet(int planet) {
		mWalk.setPlanetOn(planet);
		sListener.valueChanged(mWalk.getPercentage());
		pListener.valueChanged(mWalk.getPlanetOn());

	}

	/*
	 * Gets the current planet on
	 * @return int the current planet
	 */
	public int getPlanet() {
		return mWalk.getPlanetOn();
	}

	/*
	 * Gets the progress to the next planet
	 * @return float the progress
	 */
	public float getProgress() {
		return mWalk.getPercentage();
	}

	/*
	 * 
	 */
	public void reloadSettings() {

		mWalk.reInit(mSettings.getDistance(), mSettings.getStartPlanet(),
				mSettings.getEndPlanet());

		pListener.valueChanged(mWalk.getPlanetOn());
	}

	/*
	 * This method is called every time a step is taken 
	 */
	public void onStep() {

		try {
			// number of miles

			// update distance
			mWalk.setDistance( mWalk.getDistance() +(float) (// miles
			STEP_LENGTH // inches
			/ 63360.0)); // inches/mile
			
			// let the listener know that the progress bar should
			// be updated
			
			if (mWalk.getPercentage() > 1) {
			
				// play the new podcast
				// reset the miles
				mWalk.setDistance(0);
				// increment the planet

				mWalk.setPlanetOn(mWalk.getPlanetOn() + 1);
				// let the main activity know (to update to the next planet)
				pListener.valueChanged(mWalk.getPlanetOn());
			}

			sListener.valueChanged(mWalk.getPercentage());
		} catch (ArrayIndexOutOfBoundsException ex) {

		}
	}

	/*
	 * This is a deprecated function
	 */
	public void passValue() {
		// As per the other notifier functions, this is not needed
	}
}
