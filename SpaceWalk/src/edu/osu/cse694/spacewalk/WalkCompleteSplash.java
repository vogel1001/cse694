package edu.osu.cse694.spacewalk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
//import android.util.Log;
import android.view.MotionEvent;

/**
 * 
 */
public class WalkCompleteSplash extends Activity {

		/*
		 * splashWait: Number of ms to wait on the splashscreen
		 */
		private static final int splashWait = 5000;

		/*
		 * launchMain: The runnable that will launch the main screen
		 */
		private Runnable launchMain;
		
		/*
		 * launchMainHandler: The handler responsable to starting launchmain
		 */
		private Handler launchMainHandler;

		/*
		 * The planet that has been reached.  Informs the display.
		 */
		private int planetArrivedAt;
		
		/*
		 * Determines if the phone will vibrate
		 */
		private SharedPreferences settings;

		/**
		 * 
		 * @param savedInstanceState
		 */
		@Override
		public void onCreate(Bundle savedInstanceState) {

			super.onCreate(savedInstanceState);
			//Log.d("SPLASH", "SPLASH");
			setContentView(R.layout.walk_complete_splash);
			settings = PreferenceManager.getDefaultSharedPreferences(this);
			launchMainHandler = new Handler();

			planetArrivedAt = getIntent().getExtras().getInt("pager_tab_number");

			launchMain = new Runnable() {
				public void run() {
					launchNextActivity();
				}
			};
			if (!settings.getString(SettingsActivity.NOTIFICATION, "0").equals("0")) {
				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

				// Vibrate for 300 milliseconds
				v.vibrate(800);
			}
			launchMainHandler.postDelayed(launchMain, splashWait);

		}

		/**
		 * Return to the WalkingActivity with a slightly modified UI: the Upcoming Planet tab will now
		 * display that the walk is complete
		 */
		public void launchNextActivity() {
			
				Intent launch = new Intent(this, WalkingActivity.class);
				launch.putExtra("pager_tab_number", planetArrivedAt);
				startActivity(launch);
				finish();
			
		}

		/**
		 * 
		 */
		public void onPause() {
			super.onPause();
			// remove the timer from the running stack so we don't call the main
			// program after the user has exited the splash screen.
			launchMainHandler.removeCallbacks(launchMain);
			finish();
		}

		/**
		 * The splash screen will be destroyed on a finger touch
		 * @param event  a finger touch
		 */
		public boolean onTouchEvent(MotionEvent event) {
			// if the user touches the screen, skip right to the main activity
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				launchMainHandler.removeCallbacks(launchMain);
				launchNextActivity();
			}
			return true;
		}

	
}
