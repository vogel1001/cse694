package edu.osu.cse694.spacewalk.pedometer.service;

/**
 * Stores information about the current walk
 * 
 * @author David Burl
 * 
 */

public class Walk {
	/*
	 * distances to the from PLANET_DISTANCE[n-1] to PLANET_DISTANCE[n] * 10
	 */
	public static int[] PLANET_DISTANCE = { 3598361, 3124875, 2572474, 4867820,
			34199670, 40455600, 89576200, 101489200 };
	
	/*
	 * Distance to the next planet
	 */
	private float distance;

	/*
	 * planet last visited
	 */
	private int planetOn;

	/*
	 * total distance to travel (on earth)
	 */
	private float totalDistEarth;

	/*
	 * total distance to travel (in solar system)
	 */
	private int totalDistSolar;

	/*
	 * percentage of each planet in the journey
	 */
	private float planetMiles[];

	/*
	 * the start planet of the walk
	 */
	private int sPlanet;
	
	public Walk(float earthDist, int startPlanet, int endPlanet) {
		reInit(earthDist, startPlanet, endPlanet);
		sPlanet = startPlanet;

	}

	public void reInit(float earthDist, int startPlanet, int endPlanet) {
		// set the earth distance
		totalDistEarth = earthDist;
		// calculate the solar distance
		totalDistSolar = 0;
		for (int i = startPlanet; i < endPlanet; i++) {
			totalDistSolar += Walk.PLANET_DISTANCE[i];
		}
		//
		planetMiles = new float[endPlanet - startPlanet];
		for (int i = 0; i < endPlanet - startPlanet; i++) {
			planetMiles[i] = (((float) Walk.PLANET_DISTANCE[startPlanet + i]) / ((float) totalDistSolar));
			planetMiles[i] = planetMiles[i] * totalDistEarth;
		}
	}

	/*
	 * Accessor Method for Distance
	 */
	public float getDistance() {
		return distance;
	}

	/*
	 * Setter method for Distance
	 */
	public void setDistance(float dist) {
		distance = dist;
	}

	/*
	 * Accessor method for the current planet on
	 */
	public int getPlanetOn() {
		return planetOn;
	}

	/*
	 * Setter method for the current planet on
	 */
	public void setPlanetOn(int planet) {
		planetOn = planet;
	}

	/*
	 * Setter method for the distance to travel on earth
	 */
	public void setEarthDistance(float earthDist) {
		totalDistEarth = earthDist;
	}

	/*
	 * Accessor method for the distance to travel on earth
	 */
	public float getEarthDist() {
		return totalDistEarth;
	}
	

	/*
	 * Accessor method for how far to travel (in the solar system miles)
	 */
	public int getSolarDistToTravel() {
		return totalDistSolar;
	}

	/*
	 * Accessor method for the planet miles
	 */
	public float[] getPlanetMiles() {
		return planetMiles;
	}
	
	public float getPercentage() {
		return distance / planetMiles[planetOn - sPlanet];
	}

}
