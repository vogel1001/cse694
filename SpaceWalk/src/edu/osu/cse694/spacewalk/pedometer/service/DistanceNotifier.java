/*
 *  Pedometer - Android App
 *  Copyright (C) 2009 Levente Bagi
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.osu.cse694.spacewalk.pedometer.service;


/**
 * Calculates and displays the distance walked.
 * This is used as a blackbox class.
 * 
 * @author Levente Bagi
 */
public class DistanceNotifier implements StepListener {

	public interface Listener {
		public void valueChanged(float value);

		public void passValue();
	}

	private Listener mListener;

	float mDistance = 0;

	public DistanceNotifier(Listener listener) {
		mListener = listener;
		reloadSettings();
	}

	public void setDistance(float distance) {
		mDistance = distance;
		notifyListener();
	}

	public void reloadSettings() {
		notifyListener();
	}

	public void onStep() {
		mDistance += (float) (// miles
		25 // inches
		/ 63360.0); // inches/mile

		notifyListener();
	}

	private void notifyListener() {
		//Log.d("DistanceNotifier", "" + mDistance);

		mListener.valueChanged(mDistance);
	}

	public void passValue() {
		// Callback of StepListener - Not implemented
	}

	public void playPodcast() {

	}

}
