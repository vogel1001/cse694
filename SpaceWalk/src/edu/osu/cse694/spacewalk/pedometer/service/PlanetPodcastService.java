package edu.osu.cse694.spacewalk.pedometer.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
//import android.util.Log;

/*
 * 
 */
public class PlanetPodcastService extends Service {

	/*
	 * 
	 */
	public static String TAG = "PlanetPodcastService";
	
	/*
	 * 
	 */
	public String instruction = "default";
	
	/*
	 * 
	 */
	private static PodcastPlayer podcastPlayer;
	
	/*
	 * 
	 * @param intent
	 * @return IBinder
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	/*
	 * 
	 */
	@Override
	public void onCreate() {
		//Toast.makeText(this, "My Service Created", Toast.LENGTH_LONG).show();
		//Log.d(TAG, "onCreate");
		podcastPlayer = PodcastPlayer.getInstance(this);
	}

	/*
	 * 
	 */
	@Override
	public void onDestroy() {
		//pausePlanet();
		//Log.d(TAG, "onDestroy");
	}
	
	/*
	 * 
	 * @param intent
	 * @param startid
	 */
	@Override
	public void onStart(Intent intent, int startid) {
		//Log.d(TAG, "onStart");
		
		podcastPlayer = PodcastPlayer.getInstance(this);
		String planet;
		
		planet = intent.getExtras().getString("playPlanet");
		if( planet != null ) {
			playPlanet(Integer.parseInt(planet));
			Log.d(TAG, "playPlanet " + planet);
			return;
		} else {
			planet = intent.getExtras().getString("pausePlanet");
			if( planet != null ) {
				pausePlanet();
				Log.d(TAG, "pausePlanet");
				return;
			} else {
				planet = intent.getExtras().getString("stopPlanet");
				if( planet != null ) {
					stopPlanet(Integer.parseInt(planet));
					Log.d(TAG, "stopPlanet " + planet);
					return;
				} else {
					planet = intent.getExtras().getString("destroy");
					if( planet != null ) {
						destroy();
						Log.d(TAG, "stopPlanet " + planet);
						return;
					}
				}
			}
		}
	}
	
	/*
	 * 
	 * @param planetNumber
	 */
	private void playPlanet( int planetNumber ) {
		podcastPlayer.playPlanet(planetNumber);
	}
	
	/*
	 * 
	 */
	private void pausePlanet() {
		podcastPlayer.pausePlanet();
		this.stopSelf();
	}
	
	/*
	 * 
	 * @param planetNum
	 */
	private void stopPlanet(int planetNum) {
		podcastPlayer.stopPlanet(planetNum);
		this.stopSelf();
	}
	
	/*
	 * 
	 */
	private void destroy() {
		podcastPlayer.destroy();
		this.stopSelf();
	}

}
