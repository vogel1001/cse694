package edu.osu.cse694.spacewalk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.InputType;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Walk settings are selected by the user; afterwards, the walk begins.
 */
public class SettingsActivity extends PreferenceActivity implements
		OnClickListener, OnSharedPreferenceChangeListener {

	
	
	/*
	 * 
	 */
	private static final String SELECTED_PLANET_SUMMARY = "Currently Selected: ";
	
	/*
	 * 
	 */
	private Button startButton;
	
	/*
	 * 
	 */
	public static final String KEY_START_PLANET_PREFERENCE = "start_planet";
	
	/*
	 * 
	 */
	public static final String KEY_END_PLANET_PREFERENCE = "end_planet";
	
	/*
	 * 
	 */
	public static final String KEY_DISTANCE_TO_TRAVEL_PREFERENCE = "distance";
	
	/*
	 * 
	 */
	public static final String NOTIFICATION = "notification";
	
	
	/*
	 * 
	 */
	public static final String PLANET_AUDIO = "planet_audio";
	
	/*
	 * 
	 */
	private ListPreference mStartPlanetPreference;
	
	/*
	 * 
	 */
	private ListPreference mEndPlanetPreference;
	
	/*
	 * 
	 */
	private EditTextPreference mDistancePreference;

	/*
	 * 
	 */
	private ListPreference mNotificationPreference;
	
	/*
	 * 
	 */
	private CheckBoxPreference mPlanetAudioPreference;
	
	/*
	 * 
	 */
	private SharedPreferences settings;
	
	/*
	 * 
	 */
	private static String[] planetNames = { "Sun", "Mercury", "Venus", "Earth",
			"Mars", "Jupiter", "Saturn", "Uranus", "Neptune" };

	/**
	 * The user selects settings before a walk begins.  The UI content references preferences.xml.
	 * 
	 * @param savedInstanceState  
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		settings = PreferenceManager
				.getDefaultSharedPreferences(this);
		
		addPreferencesFromResource(R.xml.preferences);
		setContentView(R.layout.preference_shell);

		mDistancePreference = (EditTextPreference) findPreference(KEY_DISTANCE_TO_TRAVEL_PREFERENCE);
		mDistancePreference.getEditText().setInputType(
				InputType.TYPE_CLASS_NUMBER
						| InputType.TYPE_NUMBER_FLAG_DECIMAL);

		mStartPlanetPreference = (ListPreference) findPreference(KEY_START_PLANET_PREFERENCE);
		mEndPlanetPreference = (ListPreference) findPreference(KEY_END_PLANET_PREFERENCE);

		mNotificationPreference = (ListPreference) findPreference(NOTIFICATION);
		mPlanetAudioPreference = (CheckBoxPreference) findPreference(PLANET_AUDIO);
		startButton = (Button) findViewById(R.id.startButton);

		startButton.setOnClickListener(this);
		
		settings = PreferenceManager
				.getDefaultSharedPreferences(this);
	}

	/**
	 * 
	 */
	@Override
	protected void onResume() {
		super.onResume();

		//Log.d(settings.getString(KEY_START_PLANET_PREFERENCE, "0"), "error");
		mStartPlanetPreference.setSummary(SELECTED_PLANET_SUMMARY
				+ integerToPlanetName(Integer.parseInt(settings.getString(
						KEY_START_PLANET_PREFERENCE, "0"))));
		//Log.d(settings.getString(KEY_START_PLANET_PREFERENCE, "0"), "error");

		mEndPlanetPreference.setSummary(SELECTED_PLANET_SUMMARY
				+ integerToPlanetName(Integer.parseInt(settings.getString(
						KEY_END_PLANET_PREFERENCE, "0"))));

		mDistancePreference.setSummary("Walk Distance: "
				+ settings.getString(KEY_DISTANCE_TO_TRAVEL_PREFERENCE, "2")
				+ " miles");

		// Set up a listener whenever a key changes
		settings.registerOnSharedPreferenceChangeListener(this);
	}

	/**
	 * Launches the walk after settings have been selected.
	 */
	public void startWalk() {
		// transfer control over to the WalkingActivity
		Intent newWalk = new Intent(this, WalkingActivity.class);
		//Log.d("settingsstart",settings.getString("start_planet", "0"));
		newWalk.putExtra("pager_tab_number", Integer.parseInt(settings.getString("start_planet", "0")));
		startActivity(newWalk);

		finish();
	}

	/**
	 * 
	 * @param sharedPreferences
	 * @param key
	 */
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(KEY_START_PLANET_PREFERENCE)) {
			mStartPlanetPreference.setSummary(SELECTED_PLANET_SUMMARY
					+ integerToPlanetName(Integer.parseInt(sharedPreferences
							.getString(key, "0"))));
		} else if (key.equals(KEY_END_PLANET_PREFERENCE)) {
			mEndPlanetPreference.setSummary(SELECTED_PLANET_SUMMARY
					+ integerToPlanetName(Integer.parseInt(sharedPreferences
							.getString(key, "0"))));
		} else if (key.equals(KEY_DISTANCE_TO_TRAVEL_PREFERENCE)) {
			mDistancePreference.setSummary("Walk Distance: "
					+ sharedPreferences.getString(
							KEY_DISTANCE_TO_TRAVEL_PREFERENCE, "2") + " miles");
		} else if (key.equals(NOTIFICATION)) {

		}
	}

	/**
	 * 
	 * @param i
	 * @return String
	 */
	private String integerToPlanetName(int i) {
		return planetNames[i];
	}

	/**
	 * Set's the listener for the Start button
	 * @param v
	 */
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.startButton:
			startWalk();
			break;
		}
	}

}
