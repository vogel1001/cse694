package edu.osu.cse694.spacewalk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import edu.osu.cse694.spacewalk.pedometer.service.WalkService;

/**
 * 
 */
public class SplashActivity extends Activity {


	/*
	 * splashWait: Number of ms to wait on the splashscreen
	 */
	private static final int splashWait = 5000;

	/*
	 * launchMain: The runnable that will launch the main screen
	 */
	private Runnable launchMain;
	
	/*
	 * launchMainHandler: The handler responsable to starting launchmain
	 */
	private Handler launchMainHandler;
	

	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		launchMainHandler = new Handler();
		
		// if the walk service is already running, jump right too the walking
		if (WalkService.isRunning) {
			launchMainHandler.removeCallbacks(launchMain);
			startActivity(new Intent(this, WalkingActivity.class));
			finish();
		} else {
			// otherwise, we can wait 5 seconds and launch the main screen
			// create thread to run after the time has run out.
			launchMain = new Runnable() {
				public void run() {
					launchMainActivity();
				}
			};			
			launchMainHandler.postDelayed(launchMain, splashWait);
		}
	}

	/**
	 * called to launch the main menu
	 */
	public void launchMainActivity() {
		finish();
		startActivity(new Intent(this, SpaceWalkActivity.class));
	}

	/**
	 * 
	 */
	public void onPause() {
		super.onPause();
		// remove the timer from the running stack so we don't call the main
		// program after the user has exited the splash screen.
		launchMainHandler.removeCallbacks(launchMain);
	}

	/**
	 * The splash screen will be destroyed on a finger touch
	 * @param event  a finger touch
	 */
	public boolean onTouchEvent(MotionEvent event) {
		// if the user touches the screen, skip right to the main activity
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			launchMainHandler.removeCallbacks(launchMain);
			launchMainActivity();
		}
		return true;
	}

}