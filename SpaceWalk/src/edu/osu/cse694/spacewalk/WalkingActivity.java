package edu.osu.cse694.spacewalk;

import static edu.osu.cse694.spacewalk.SettingsActivity.KEY_START_PLANET_PREFERENCE;

import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
//import android.util.Log;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import edu.osu.cse694.spacewalk.pedometer.service.WalkService;

/**
 * The core of the application runs in this activity.  It manages the in-walk screen consisting of the Visited Planets, Upcoming
 * Planet and Map tabs.  The WalkService and PlanetPodcastService also branch from this activity.
 *
 */
public class WalkingActivity extends MapActivity {

	/*
	 * Reference to data of a previous walk; this data is used if this walk is an instance
	 * of a previous walk (if Continue Walk was chosen by the user)
	 */
	private static final String CONTINUE_WALK_SHARED_PREFERENCE = "ContinueWalk";
	
	private static final String GEOPOINTS_SHARED_PREFERENCE = "Geopoints";
	
	/*
	 * This attribute of CONTINUE_WALK_SHARED_PREFERENCE determines if this walk is
	 * an instance of a previous walk (if Continue Walk was chosen by the user)
	 */
	private static final String CONTINUE_WALK = "continue_walk";
	
	/*
	 * Use for debugging purposes
	 */
	private static final String TAG = "WalkingActivity";

	/*
	 * The settings the user selected before beginning this walk
	 */
	private SharedPreferences mSettings;
	
	/*
	 * The data of the previous walk (if needed)
	 */
	private SharedPreferences continueWalk;
	
	
	/*
	 * The planet departed from
	 */
	private int startPlanet;
	
	/*
	 * Reference of the most recent planet reached
	 */
	private int mostRecentPlanetReached = 0;
	
	/*
	 * The Context of this activity
	 */
	private Context walkActivityContext;
	
	/*
	 * Determines the state of the ViewPager in the Visited Planets tab
	 */
	private int goHere;
	
	/*
	 * The agent of the podcast service
	 */
	private PodcastServiceDelegate podcastDelegate;
	
	/*
	 * True if the user specified in the walk settings to
	 * launch planet audio when a planet is reached
	 */
	private boolean playAudioAtReachedPlanet;
	
	/*
	 * The list of real-world coordinates where the
	 * user arrived at planets; used to populate the map
	 * with planet thumbnails
	 */
	private GeoPoint[] points;

	/**
	 * 
	 * @param savedInstanceState
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		walkActivityContext = this;
		
		podcastDelegate = PodcastServiceDelegate.getPodcastServiceDelegate(this);
		
		points = new GeoPoint[9];
		
		//the walk settings that were defined by the user before the walk began
		mSettings = PreferenceManager.getDefaultSharedPreferences(this);

		playAudioAtReachedPlanet = mSettings.getBoolean(SettingsActivity.PLANET_AUDIO, false);
		/*
		the walk data from the most recent walk; this data will be used if the user
		chose to continue a walk rather than start a new one
		*/ 
		continueWalk = getSharedPreferences(CONTINUE_WALK_SHARED_PREFERENCE, MODE_PRIVATE);
				
		if (getIntent().getExtras() != null) {
			/*
			the current planet, passed in from ArrivedAtPlanetSplashActivity; used to populate
			the data pages of previously-reached planets 
			*/
			goHere = getIntent().getExtras().getInt("pager_tab_number");
			if( goHere == Integer.parseInt(mSettings.getString(SettingsActivity.KEY_START_PLANET_PREFERENCE, "0")) ) {
				if( playAudioAtReachedPlanet ) {
					podcastDelegate.playPlanet(goHere);
				}
			}
			mostRecentPlanetReached = goHere;
			if (goHere < Integer.parseInt(mSettings
					.getString("end_planet", "0"))) {
				//if the walk isn't over, use this UI
				setContentView(R.layout.main);
			} else {
				//if the last planet has been reached, use this other UI
				setContentView(R.layout.walk_complete);
			}
		} else {
			goHere = 0;
			setContentView(R.layout.main);
		}

		TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup();

		TabHost.TabSpec spec;

		//Visited Planets tab; a collection of data pages of previously-reached planets
		spec = tabHost.newTabSpec("visited_planets")
				.setIndicator("Visited Planets").setContent(R.id.visit_planets);
		tabHost.addTab(spec);

		//Upcoming Planet tab.  Name, image, and progress towards the upcoming planet
		spec = tabHost.newTabSpec("upcoming_planet")
				.setIndicator("Upcoming Planet")
				.setContent(R.id.upcoming_planet);
		tabHost.addTab(spec);

		/*
		Map tab.  Summarizes the walk, with planet images placed on the map at the locations
		where the user reached them
		*/
		spec = tabHost.newTabSpec("the_map").setIndicator("Map")
				.setContent(R.id.the_map);
		tabHost.addTab(spec);
		
		/*
		 * If the last planet hasn't been reached, the upcoming planet needs to be defined
		 */
		startPlanet = Integer.parseInt(mSettings.getString(
				KEY_START_PLANET_PREFERENCE, "0"));
		
		onCreateVisited(goHere);
		if (goHere < Integer.parseInt(mSettings.getString("end_planet", "0"))) {
			onCreateUpcomingPlanet();
		}
		onCreateMap();

		
		//Log.d("START", Integer.toString(startPlanet));
		tabHost.setCurrentTab(0);
	}


	/**
	 *
	 */
	@Override
	protected void onResume() {
		super.onResume();
		//Log.d("WalkingActivityOnResume", "WalkingActivityOnResume");
		if (goHere < Integer.parseInt(mSettings.getString("end_planet", "0"))) {
			onResumeUpcomingPlanet();
		}
		onResumeMap();

	}

	/**
	 * 
	 */
	@Override
	protected void onPause() {
		super.onPause();
		//Log.d("WalkingActivityOnPause", "WalkingActivityOnPause");
		onPauseUpcomingPlanet();
	}

	// CODE FOR UPCOMING PLANETS

	/*
	 * Displays the total distance walked in the Upcoming Planet tab
	 */
	private TextView mDistanceValueView;
	
	/*
	 * Displays the upcoming planet name in the Upcoming Planet tab
	 */
	private TextView mUpcomingPlanetName;
	
	/*
	 * Displays the upcoming planet's image in the Upcoming Planet tab
	 */
	private ImageView mUpcomingImage;
	
	/*
	 * Displays the progress towards the next planet in the Upcoming Planet tab
	 */
	private ProgressBar mProgressValueView;
	
	/*
	 * The current step value
	 */
	private int mStepValue;
	
	/*
	 * The last planet reached
	 */
	private int mPlanetOn;
	
	/*
	 * The progress 
	 */
	private int mProgress;
	
	/*
	 * 
	 */
	private float mDistanceValue;
	
	/*
	 * 
	 */
	private boolean mQuitting = false;

	/*
	 * true when service is running
	 */
	private boolean mIsRunning;
	
	/*
	 * Pedometer service
	 */
	private WalkService mService;

	/**
	 * Called in on resume to correctly restore the Upcoming Planet tab and re-launch
	 * the WalkService
	 */
	private void onResumeUpcomingPlanet() {

		// Read from preferences if the service was running on the last onPause
		mIsRunning = WalkService.isRunning;

		// Start the service if this is considered to be an application start
		// (last onPause was long ago)
		if (!mIsRunning) {
			startStepService();
			bindStepService();
		} else if (mIsRunning) {
			bindStepService();
		}
		mUpcomingImage = (ImageView) findViewById(R.id.upcoming_image);
		mDistanceValueView = (TextView) findViewById(R.id.distance_value);
		mProgressValueView = (ProgressBar) findViewById(R.id.progress_to_next_planet_view);
		mUpcomingPlanetName = (TextView) findViewById(R.id.upcoming_planet_name);
		mProgressValueView.setMax(1000);
		if (continueWalk.getBoolean(CONTINUE_WALK, false)
				&& mostRecentPlanetReached == continueWalk.getInt("planet", 0)) {
			//Log.d("HHHDHSH", "SETPROGRESS");
			mProgressValueView.setProgress((int) (1000 * continueWalk.getFloat(
					"progress", 0.0F)));
		}

	}

	/**
	 * Called in onCreate to correctly create the Upcoming Planet UI
	 */
	private void onCreateUpcomingPlanet() {
		if (mIsRunning) {
			mProgressValueView.setProgress((mService.getProgress()));
			mUpcomingImage.setImageResource(Planet.LARGE_PICTURE_FILES[mService
					.getPlanet() + 1]);
			//Log.d("Here's your image", Integer.toString(mService.getPlanet()));
		}
	}

	/**
	 * Stops the pedometer service
	 */
	private void onPauseUpcomingPlanet() {
		if (mIsRunning) {
			unbindStepService();
		}

	}

	/*
	 * 
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		
		/**
		 * 
		 * @param className
		 * @param service
		 */
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = ((WalkService.StepBinder) service).getService();
			mService.registerCallback(mCallback);
			mService.reloadSettings();
		}

		/**
		 * 
		 * @param className
		 */
		public void onServiceDisconnected(ComponentName className) {
			mService = null;
		}
	};

	/**
	 * Starts the pedometer service
	 */
	private void startStepService() {
		if (!mIsRunning) {
			//Log.i(TAG, "startStepService");
			mIsRunning = true;
			startService(new Intent(WalkingActivity.this, WalkService.class));
		}
	}

	/**
	 * Binds the pedometer service to this activity
	 */
	private void bindStepService() {
		//Log.i(TAG, "bindStepService");
		getApplicationContext().bindService(
				new Intent(WalkingActivity.this, WalkService.class),
				mConnection,
				Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
	}

	/**
	 * Unbinds the pedometer service from this activity
	 */
	private void unbindStepService() {
		Log.i(TAG, "[SERVICE] Unbind");
		getApplicationContext().unbindService(mConnection);
	}

	/**
	 * Stops the pedometer service
	 */
	private void stopStepService() {
		//Log.i(TAG, "[SERVICE] Stop");
		if (mService != null) {
			Log.i(TAG, "[SERVICE] stopService");
			stopService(new Intent(WalkingActivity.this, WalkService.class));
		}
		mIsRunning = false;
	}

	/*
	 * 
	 */
	// TODO: unite all into 1 type of message
	private WalkService.ICallback mCallback = new WalkService.ICallback() {
		
		/**
		 * 
		 * @param value
		 */
		public void stepsChanged(int value) {
			mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
		}
		
		/**
		 * 
		 * @param value
		 */
		public void distanceChanged(float value) {
			mHandler.sendMessage(mHandler.obtainMessage(DISTANCE_MSG,
					(int) (value * 1000), 0));
		}
		
		/**
		 * 
		 * @param value
		 */
		public void planetChanged(int value) {
			mHandler.sendMessage(mHandler.obtainMessage(PLANET_MSG, value, 0));
		}
		
		/**
		 * 
		 * @param value
		 */
		public void progressChanged(float value) {
			mHandler.sendMessage(mHandler.obtainMessage(PROGRESS_MSG,
					(int) (value * 1000), 0));

		}
	};

	/*
	 * 
	 */
	private static final int STEPS_MSG = 1;
	
	/*
	 * 
	 */
	private static final int PLANET_MSG = 2;
	
	/*
	 * 
	 */
	private static final int DISTANCE_MSG = 3;
	
	/*
	 * 
	 */
	private static final int PROGRESS_MSG = 4;

	/*
	 * 
	 */
	private Handler mHandler = new Handler() {
		
		/**
		 * 
		 * @param msg
		 */
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case PLANET_MSG:
				mPlanetOn = (int) msg.arg1;
				if (mPlanetOn > Integer.parseInt(mSettings.getString(
						"end_planet", "0"))) {
					//Log.d(Integer.toString(mPlanetOn),"mplanetondsfasd");
					unbindStepService();
					stopStepService();
				}
				//Log.d(Integer.toString(mPlanetOn),"mplanetondsfasd");
				// addPointAddCurrLocation(mPlanetOn);
				points = mService.getPlanetLocationsOnEarth();
				SharedPreferences geopointsPreference = getSharedPreferences(GEOPOINTS_SHARED_PREFERENCE, MODE_PRIVATE);
				SharedPreferences.Editor editor = geopointsPreference.edit();
				editor.clear();
				int k = 0;
				for( int i = 0; i < 9; i++ ) {
					if( points[i] != null ) {
						editor.putInt(Integer.toString(k), points[i].getLatitudeE6());
					} else {
						editor.putInt(Integer.toString(k), Integer.MAX_VALUE);
					}
					k+=1;
					if( points[i] != null ) {
						editor.putInt(Integer.toString(k), points[i].getLongitudeE6());
					} else {
						editor.putInt(Integer.toString(k), Integer.MAX_VALUE);
					}
					k+=1;
				}
				editor.commit();
				
				updateMap();
				if( mPlanetOn < Integer.parseInt(mSettings.getString(
						"end_planet", "0")) ) {
				mUpcomingImage
						.setImageResource(Planet.LARGE_PICTURE_FILES[mPlanetOn + 1]);
				mUpcomingPlanetName.setText(Planet.PLANET_NAMES[mPlanetOn + 1]);
				}
				
				if (mostRecentPlanetReached == (mPlanetOn - 1)) {
					mostRecentPlanetReached = mPlanetOn;
					Intent splashIntent = new Intent(walkActivityContext,
							ArrivedAtPlanetSplashActivity.class);
					if( playAudioAtReachedPlanet ) {
						//Log.d("I SHOUDL BE PLAYING AUDIO HERE","SODSA");
						podcastDelegate.playPlanet(mostRecentPlanetReached);
					}
					splashIntent.putExtra("arrival", mPlanetOn);
					startActivity(splashIntent);
					finish();
				} else {
					mostRecentPlanetReached = mPlanetOn;
				}

				// Uncomment to see toast of GPS coords in this format:
				// Planet: 0
				// Location: (latitude), (logitude)
				// Planet: 1
				// Location: not yet reached
				// Planet: ...
				// Location: ...
				// (shortcut to uncomment: highlight lines you want to
				// uncomment, and do a 'command + /' on Mac)
				//
				// StringBuilder builder = new StringBuilder();
				//
				// for (int i = 0; i <= 8; i++) {
				// builder.append("Planet: " + i + "\n");
				// if (points[i] != null) {
				// builder.append("Location: " + points[i].getLatitudeE6() +
				// ", " + points[i].getLongitudeE6() + "\n");
				// } else {
				// builder.append("Location: not yet reached \n");
				// }
				// }
				//
				// Toast.makeText(WalkingActivity.this, builder,
				// Toast.LENGTH_LONG).show();

				break;
			case PROGRESS_MSG:
				mProgress = (int) msg.arg1;
				mProgressValueView.setProgress(mProgress);
				break;
			case STEPS_MSG:
				mStepValue = (int) msg.arg1;
				//Log.d("STEPS_MSG", Integer.toString(msg.arg1));
				break;
			case DISTANCE_MSG:
				mDistanceValue = ((int) msg.arg1) / 1000f;
				//Log.d("DISTANCE_MSG", Integer.toString(msg.arg1));
				if (mDistanceValue <= 0) {
					mDistanceValueView.setText("0");
				} else {
					mDistanceValueView
							.setText(("" + (mDistanceValue + 0.000001f))
									.substring(0, 5));
				}
				break;
			default:
				super.handleMessage(msg);
			}
		}

	};

	/*
	 * 
	 */
	private static final int MENU_QUIT = 9;
	
	/*
	 * 
	 */
	private static final int MENU_PAUSE = 1;
	
	/*
	 * 
	 */
	private static final int MENU_RESUME = 2;

	/**
	 * Creates the menu items
	 */
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		/*
		 * if (mIsRunning) { menu.add(0, MENU_PAUSE, 0, R.string.pause)
		 * .setIcon(android.R.drawable.ic_media_pause) .setShortcut('1', 'p'); }
		 * else { menu.add(0, MENU_RESUME, 0, R.string.resume)
		 * .setIcon(android.R.drawable.ic_media_play) .setShortcut('1', 'p'); }
		 */
		menu.add(0, MENU_QUIT, 0, R.string.quit)
				.setIcon(android.R.drawable.ic_lock_power_off)
				.setShortcut('9', 'q');
		return true;
	}

	/**
	 * Handles item selections 
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_PAUSE:
			unbindStepService();
			stopStepService();
			return true;
		case MENU_RESUME:
			startStepService();
			bindStepService();
			return true;
		case MENU_QUIT:
			podcastDelegate.destroy();
			if (goHere < Integer.parseInt(mSettings
					.getString("end_planet", "0"))) {
				mQuitting = true;
				SharedPreferences continueWalk = getSharedPreferences(
						CONTINUE_WALK_SHARED_PREFERENCE, MODE_PRIVATE);
				SharedPreferences.Editor editor = continueWalk.edit();
				editor.clear();
				editor.putInt("steps", mService.getCurrentSteps());
				Log.d("put_steps", Integer.toString(mService.getCurrentSteps()));
				editor.putFloat("distance", mService.getCurrentDistance());
				//Log.d("put_distance",
						//Float.toString(mService.getCurrentDistance()));
				editor.putFloat("progress", mService.getCurrentProgress());
				//Log.d("put_progress",
						//Float.toString(mService.getCurrentProgress()));
				editor.putInt("planet", mService.getCurrentPlanet());
				//Log.d("put_planet",
						//Integer.toString(mService.getCurrentPlanet()));
				editor.putBoolean(CONTINUE_WALK, true);
				editor.commit();
				unbindStepService();
				stopStepService();
			} else {
				SharedPreferences.Editor editor = continueWalk.edit();
				editor.clear();
				editor.putBoolean(CONTINUE_WALK, false);
				editor.commit();
				SharedPreferences geopointsPreference = getSharedPreferences(GEOPOINTS_SHARED_PREFERENCE, MODE_PRIVATE);
				SharedPreferences.Editor geoPointsEditor = geopointsPreference.edit();
				geoPointsEditor.clear();
			}
			finish();
			//System.exit(0);
			return true;
		}
		return false;
	}

	// CODE FOR VISITED PLANETS.

	/*
	 * 
	 */
	private ViewPager vPager;

	/**
	 * 
	 * @param currentPlanet
	 */
	private void onCreateVisited(int currentPlanet) {
		mostRecentPlanetReached = currentPlanet;
		vPager = (ViewPager) findViewById(R.id.viewpager);
		vPager.setAdapter(new ScrollAdapter());
		//Log.d("mostRecentPlanetReached",Integer.toString(mostRecentPlanetReached));
		//Log.d("startPlanet",Integer.toString(startPlanet));
		vPager.setCurrentItem(mostRecentPlanetReached-startPlanet);

	}

	/**
	 * 
	 */
	private class ScrollAdapter extends PagerAdapter implements OnClickListener {

		/**
		 * 
		 * @return int
		 */
		@Override
		public int getCount() {
			//Log.d("getCount",Integer.toString(mostRecentPlanetReached + 1 - startPlanet));
			return mostRecentPlanetReached + 1 - startPlanet; 
		}
		
		/**
		 * 
		 * @param container
		 * @param position
		 * @return Object
		 */
		public Object instantiateItem(ViewGroup container, int position) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.planet_page, null);
			WebView planetPage = (WebView) view.findViewById(R.id.page);
			planetPage.getSettings().setLayoutAlgorithm(
					LayoutAlgorithm.SINGLE_COLUMN);
			Button playPodcastButton = (Button) view
					.findViewById(R.id.play_pause);
			playPodcastButton.setOnClickListener(this);

			String htmlFile = null;

			htmlFile = Planet.HTML_FILES[position + startPlanet];
			//Log.d("position",Integer.toString(position));
			planetPage.loadUrl("file:///android_asset/" + htmlFile);
			//Log.d("htmlfile","file:///android_asset/" + htmlFile);
			//Log.d("made it", "made it");

			((ViewPager) container).addView(view, 0);
			return view;
		}

		/**
		 *
		 * @param src
		 */
		public void onClick(View src) {
			switch (src.getId()) {
			case R.id.play_pause:
				podcastDelegate.togglePlayPodcast(vPager.getCurrentItem()
						+ startPlanet);
				break;
			}
		}

		/**
		 * 
		 * @param view
		 * @param object
		 * @return boolean
		 */
		@Override
		public boolean isViewFromObject(View view, Object object) {
			return (view == ((View) object));
		}

		/**
		 * 
		 * @param container
		 * @param position
		 * @param object
		 */
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
			//Log.d("destroyItem", "destroyItem");
		}

		/**
		 * 
		 * @param container
		 */
		@Override
		public void finishUpdate(ViewGroup container) {
			//Log.d("finishUpdate", "finishUpdate");
		}

		/**
		 * 
		 * @param state
		 * @param loader
		 */
		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
			//Log.d("restoreState", "restoreState");
		}

		/**
		 * 
		 * @return Parceable
		 */
		@Override
		public Parcelable saveState() {
			//Log.d("saveState", "saveState");
			return null;
		}

		/**
		 * 
		 * @param container
		 */
		@Override
		public void startUpdate(View container) {
			//Log.d("stateUpdate", "startUpdate");
		}

	}

	// MAP TAB:

	/*
	 * 
	 */
	private MapController mapController;
	
	/*
	 * 
	 */
	private MapView map;
	
	/*
	 * 
	 */
	private int mapZoom = 17;
	

	/**
	 * 
	 * @param planetLocations
	 */
	private void updateMap() {

		// Toast.makeText(WalkingActivity.this, builder,
		// Toast.LENGTH_LONG).show();

		SharedPreferences geoPoints = getSharedPreferences(GEOPOINTS_SHARED_PREFERENCE, MODE_PRIVATE);
		//Testing to see if the GeoPoints SharedPreference has been populated yet; if not abort the method
		if( geoPoints.getInt("0", Integer.MIN_VALUE) == Integer.MIN_VALUE ) {
			map.invalidate();
			return;
		}
		
		GeoPoint[] planetLocations = new GeoPoint[9];
		int k = 0;
		for( int i = 0; i < 9; i++ ) {
			int latitude = geoPoints.getInt(Integer.toString(k), Integer.MAX_VALUE);
			k+=1;
			int longitude = geoPoints.getInt(Integer.toString(k), Integer.MAX_VALUE);
			k+=1;
			planetLocations[i] = new GeoPoint(latitude,longitude);
		}
		
		List<Overlay> allOverlays = map.getOverlays();
		allOverlays.clear();
		for (int i = 0; i < 9; i++) {
			if (planetLocations[i].getLatitudeE6() != Integer.MAX_VALUE) {
				//Log.d("MAP", "Adding point"+planetLocations[i].getLatitudeE6()+","+planetLocations[i].getLongitudeE6());
				allOverlays.add(new PlanetOverlay(planetLocations[i], i));
				mapController.animateTo(planetLocations[i]);
			}
		}
		

		map.invalidate();
	}

	/**
	 * 
	 */
	private void onCreateMap() {
		// 8 planets + sun
		
		map = (MapView) findViewById(R.id.map);
		map.setSatellite(true);
		map.setBuiltInZoomControls(true);

		mapController = map.getController();
		mapController.setZoom(mapZoom);
		
		updateMap();
	}

	/**
	 * 
	 * @return boolean
	 */
	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 */
	private void onResumeMap() {

		map.setSatellite(true);
		// get MapController that helps to set/get location, zoom etc.
		mapController = map.getController();
		mapController.setZoom(mapZoom);
		
		updateMap();

		// addPointAddCurrLocation(mostRecentPlanetReached);
		// animate to the most recent point
		// mapController.animateTo(planetPoints[mostRecentPlanetReached]);
	}

	/**
	 * 
	 */
	class PlanetOverlay extends Overlay {
		
		/*
		 * 
		 */
		GeoPoint mGeo;
		
		/*
		 * 
		 */
		int planet;

		/**
		 * 
		 * @param g
		 * @param p
		 */
		public PlanetOverlay(GeoPoint g, int p) {
			planet = p;
			mGeo = g;
		}

		/**
		 * 
		 * @param canvas
		 * @param mapView
		 * @param shadow
		 * @param when
		 */
		@Override
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow,
				long when) {
			super.draw(canvas, mapView, shadow);
			Point screenPts = new Point();
			mapView.getProjection().toPixels(mGeo, screenPts);
			Bitmap bmp = BitmapFactory.decodeResource(getResources(),
					Planet.SMALL_PICTURE_FILES[planet]);
			canvas.drawBitmap(bmp, screenPts.x, screenPts.y, null);
			return true;
		}
	}

}
